"""
An implementation of median sort. It's like quicksort, but worse.

>>> median_sort([3, 1, 4, 1, 5, 9])
[1, 1, 3, 4, 5, 9]
>>> median_sort([])
[]
>>> median_sort(['a', 'b', 'b', 'a'])
['a', 'a', 'b', 'b']
"""

from math import floor

from partition import partition

def find_nth(data, target):
    """
    Find what the Nth value of a list would be, if it were in sorted order.

    data is the list to search, target is the index to look up. May partially
    sort data. The value at target will be in its correct sorted location.
    All values before target will be less than or equal to the value at target,
    and all values after target will be greater than or equal to target. No
    other guarantees are made.

    >>> find_nth([1, 2, 3], 2)
    3
    >>> find_nth([1, 2, 2], 0)
    1
    >>> data = [1, 2, 1]
    >>> find_nth(data, 1)
    1
    >>> data
    [1, 1, 2]
    >>> find_nth([2, 3, 1], 2)
    3
    >>> find_nth([1, 1], 1)
    1
    >>> find_nth([3, 2, 1], 1)
    2
    >>> find_nth([1, 2, 3], 0)
    1
    >>> find_nth([1, 2, 3], 1)
    2
    >>> find_nth([1, 2, 3], 2)
    3
    """
    return find_nth_bounded(data, target, 0, len(data))

def find_nth_bounded(data, target, left, right):
    """
    Like find_nth, but only checks a portion of the input list.

    Assumes that target is in the range [left, right), that all values before
    left are less than the final value of data[target], and all values from
    right onwards are greater than the final value of data[target].

    >>> find_nth_bounded([1, 2, 3], 2, 2, 3)
    3
    """
    assert left <= target
    assert target < right
    assert left < right
    i = partition(data, left, right)
    if i == target:
        return data[i]
    if i < target:
        return find_nth_bounded(data, target, i + 1, right)
    return find_nth_bounded(data, target, left, i)

def median_sort(data):
    """
    Median Sort. Places its argument into sorted order, and returns it.

    Median Sort finds the median value of the list, and places it at the middle
    location in the list. Along the way, all values less than the median are
    placed before it, and all values greater than the median are placed after
    it. Then, each half is sorted recursively.

    >>> median_sort([1, 2, 3])
    [1, 2, 3]
    >>> median_sort([1, 2, 2])
    [1, 2, 2]
    >>> median_sort([1, 2, 1])
    [1, 1, 2]
    >>> median_sort([2, 3, 1])
    [1, 2, 3]
    >>> median_sort([1, 1])
    [1, 1]
    >>> median_sort([3, 2, 1])
    [1, 2, 3]
    >>> median_sort([5, 4, 3, 2, 1])
    [1, 2, 3, 4, 5]
    >>> median_sort([5, 7, 8, 3, 1, 5, 9])
    [1, 3, 5, 5, 7, 8, 9]
    >>> median_sort([7, 0, 7, 5, 7, 8, 3, 1, 5, 9])
    [0, 1, 3, 5, 5, 7, 7, 7, 8, 9]
    >>> median_sort([4, 0, 8, 6, 8, 5, 4, 8, 1, 9])
    [0, 1, 4, 4, 5, 6, 8, 8, 8, 9]
    """
    return median_sort_range(data, 0, len(data))

def median_sort_range(data, left, right):
    """
    Place everything in the range [left, right) in sorted order.

    Assumes that everything before left in the list is less than anything in
    the range [left, right), and everthing from right onwards is greater than
    anything in the range [left, right). However, the above invariant applies
    whether this precondition is true or not.

    Check that base case returns without doing any work:
    >>> median_sort_range([3, 2, 1], 2, 2)
    [3, 2, 1]
    """
    if left < right:
        midpoint = floor((right - left) / 2) + left
        find_nth(data, midpoint)
        median_sort_range(data, left, midpoint)
        median_sort_range(data, midpoint + 1, right)
    return data

if __name__ == '__main__':
    import doctest
    doctest.testmod()
