from math import floor

from common import swap, conditionally_swap
from partition import partition
from insertionsort import insertion_sort_range

def quicksort(data):
    """
    Put data into sorted order and return it.

    >>> quicksort([1, 2, 3])
    [1, 2, 3]
    >>> quicksort([1, 2, 2])
    [1, 2, 2]
    >>> quicksort([1, 2, 1])
    [1, 1, 2]
    >>> quicksort([2, 3, 1])
    [1, 2, 3]
    >>> quicksort([1, 1])
    [1, 1]
    >>> quicksort([3, 2, 1])
    [1, 2, 3]
    >>> quicksort([5, 4, 3, 2, 1])
    [1, 2, 3, 4, 5]
    >>> quicksort([5, 7, 8, 3, 1, 5, 9])
    [1, 3, 5, 5, 7, 8, 9]
    >>> quicksort([7, 0, 7, 5, 7, 8, 3, 1, 5, 9])
    [0, 1, 3, 5, 5, 7, 7, 7, 8, 9]
    >>> quicksort([4, 0, 8, 6, 8, 5, 4, 8, 1, 9])
    [0, 1, 4, 4, 5, 6, 8, 8, 8, 9]
    """
    return quicksort_range(data, 0, len(data))

def quicksort_range(data, left, right):
    if right - left < 3:
        return insertion_sort_range(data, left, right)

    pick_pivot(data, left, right)
    cutoff = partition(data, left, right)
    quicksort_range(data, left, cutoff)
    quicksort_range(data, cutoff + 1, right)
    return data

def pick_pivot(data, left, right):
    """
    Picks a pivot candidate.

    Picks a pivot for the range [left, right) and moves it to right - 1.
    >>> pick_pivot([3, 2, 1], 0, 3)
    [1, 3, 2]
    >>> pick_pivot([4, 3, 2, 1], 1, 4)
    [4, 1, 3, 2]
    >>> pick_pivot([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 2, 10)
    [0, 1, 2, 3, 4, 5, 9, 7, 8, 6]
    """
    midpoint = floor((right - left) / 2) + left
    conditionally_swap(data, left, midpoint)
    conditionally_swap(data, midpoint, right - 1)
    conditionally_swap(data, left, midpoint)
    swap(data, midpoint, right - 1)
    return data

if __name__ == '__main__':
    import doctest
    doctest.testmod()
