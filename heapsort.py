from common import swap

def heapsort(data):
    """
    Put data into sorted order and return it.

    >>> heapsort([1, 2, 3])
    [1, 2, 3]
    >>> heapsort([1, 2, 2])
    [1, 2, 2]
    >>> heapsort([1, 2, 1])
    [1, 1, 2]
    >>> heapsort([2, 3, 1])
    [1, 2, 3]
    >>> heapsort([1, 1])
    [1, 1]
    >>> heapsort([3, 2, 1])
    [1, 2, 3]
    >>> heapsort([5, 4, 3, 2, 1])
    [1, 2, 3, 4, 5]
    >>> heapsort([5, 7, 8, 3, 1, 5, 9])
    [1, 3, 5, 5, 7, 8, 9]
    >>> heapsort([7, 0, 7, 5, 7, 8, 3, 1, 5, 9])
    [0, 1, 3, 5, 5, 7, 7, 7, 8, 9]
    >>> heapsort([4, 0, 8, 6, 8, 5, 4, 8, 1, 9])
    [0, 1, 4, 4, 5, 6, 8, 8, 8, 9]
    """
    build_max_heap(data)
    for i in range(len(data) - 1, -1, -1):
        swap(data, 0, i)
        max_heapify(data, 0, i)
    return data

def build_max_heap(data):
    """

    >>> build_max_heap([1, 2, 3])
    [3, 2, 1]
    >>> build_max_heap([1, 2, 2])
    [2, 1, 2]
    >>> build_max_heap([1, 2, 1])
    [2, 1, 1]
    >>> build_max_heap([2, 3, 1])
    [3, 2, 1]
    >>> build_max_heap([1, 1])
    [1, 1]
    >>> build_max_heap([3, 2, 1])
    [3, 2, 1]
    >>> build_max_heap([5, 4, 3, 2, 1])
    [5, 4, 3, 2, 1]
    >>> build_max_heap([5, 7, 8, 3, 1, 5, 9])
    [9, 7, 8, 3, 1, 5, 5]
    >>> build_max_heap([7, 0, 7, 5, 7, 8, 3, 1, 5, 9])
    [9, 7, 8, 5, 7, 7, 3, 1, 5, 0]
    >>> build_max_heap([4, 0, 8, 6, 8, 5, 4, 8, 1, 9])
    [9, 8, 8, 6, 8, 5, 4, 4, 1, 0]
    """
    size = len(data) 
    for i in range(size - 1, -1, -1):
        max_heapify(data, i, size)
    return data

def max_heapify(data, root, cutoff):
    """
    >>> max_heapify([1, 2, 3], 0, 3)
    [3, 2, 1]
    >>> max_heapify([1, 4, 3], 0, 3)
    [4, 1, 3]
    >>> max_heapify([1, 2, 3], 0, 2)
    [2, 1, 3]
    >>> max_heapify([1, 2, 3], 0, 1)
    [1, 2, 3]
    >>> max_heapify([1, 2, 3, 4], 1, 4)
    [1, 4, 3, 2]
    >>> max_heapify([1, 2, 3, 4, 5, 6], 0, 6)
    [3, 2, 6, 4, 5, 1]
    >>> max_heapify([1, 3, 2, 4], 0, 4)
    [3, 4, 2, 1]
    >>> max_heapify([9, 7, 8, 3, 1, 5, 5], 3, 7)
    [9, 7, 8, 3, 1, 5, 5]
    """
    left = 2 * (root) + 1
    right = 2 * (root) + 2

    largest = root
    if left < cutoff and data[largest] < data[left]:
        largest = left
    if right < cutoff and data[largest] < data[right]:
        largest = right
    if largest != root:
        swap(data, root, largest)
        max_heapify(data, largest, cutoff)
    return data

if __name__ == '__main__':
    import doctest
    doctest.testmod()
