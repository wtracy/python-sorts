"""
A simple insertion sort implementation.

>>> insertion_sort([3, 1, 4, 1, 5, 9])
[1, 1, 3, 4, 5, 9]
>>> insertion_sort([])
[]
>>> insertion_sort(['a', 'b', 'b', 'a'])
['a', 'a', 'b', 'b']
"""

from common import swap

def insertion_sort(data):
    """
    Put data into sorted order and return it.

    >>> insertion_sort([1, 2, 3])
    [1, 2, 3]
    >>> insertion_sort([1, 2, 2])
    [1, 2, 2]
    >>> insertion_sort([1, 2, 1])
    [1, 1, 2]
    >>> insertion_sort([2, 3, 1])
    [1, 2, 3]
    >>> insertion_sort([1, 1])
    [1, 1]
    >>> insertion_sort([3, 2, 1])
    [1, 2, 3]
    >>> insertion_sort([5, 4, 3, 2, 1])
    [1, 2, 3, 4, 5]
    >>> insertion_sort([5, 7, 8, 3, 1, 5, 9])
    [1, 3, 5, 5, 7, 8, 9]
    >>> insertion_sort([7, 0, 7, 5, 7, 8, 3, 1, 5, 9])
    [0, 1, 3, 5, 5, 7, 7, 7, 8, 9]
    >>> insertion_sort([4, 0, 8, 6, 8, 5, 4, 8, 1, 9])
    [0, 1, 4, 4, 5, 6, 8, 8, 8, 9]
    """
    return insertion_sort_range(data, 0, len(data))

def insertion_sort_range(data, left, right):
    for i in range(left, right - 1):
        j = i
        while j >= left and data[j] > data[j + 1]:
            swap(data, j, j+1)
            j = j - 1
    return data

if __name__ == '__main__':
    import doctest
    doctest.testmod()
