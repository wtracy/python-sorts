from common import swap

def partition(data, left, right):
    """
    Partition a list into sections >= a pivot value and <= the pivot.

    Operates on the range [left, right). Selects the last value as a pivot.
    """
    pivot = data[right-1]
    i = left
    for j in range(left, right):
        if data[j] < pivot:
            swap(data, i, j)
            i = i + 1
    swap(data, i, right-1)
    return i
