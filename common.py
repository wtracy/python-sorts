"""
Generic utility functions.

is_sorted() accepts a list and returns True if its contents are in ascending
order, false otherwise.

swap() accepts a list and two index values. The values in the list indicated by
the two indeces are swapped.
"""

def is_sorted(data):
    """
    Return True if the list is sorted, False otherwise.

    >>> is_sorted([1, 2, 3])
    True
    >>> is_sorted([1, 2, 2])
    True
    >>> is_sorted([1, 2, 1])
    False
    >>> is_sorted([2, 3, 1])
    False
    >>> is_sorted([1, 1])
    True
    >>> is_sorted([])
    True
    >>> is_sorted([0, 2, 1, 3])
    False
    """
    if not data:
        return True
    prev = data[0]
    for value in data:
        if value < prev:
            return False
        prev = value
    return True

def swap(data, i, j):
    """
    Swap the values at data[i] and data[j].

    >>> data = [1, 2]
    >>> swap(data, 0, 1)
    >>> data
    [2, 1]
    >>> swap(data, 0, 0)
    >>> data
    [2, 1]
    >>> data = [5, 7, 8, 3, 1, 5, 9]
    >>> swap(data, 1, 3)
    >>> data
    [5, 3, 8, 7, 1, 5, 9]
    """
    temp = data[i]
    data[i] = data[j]
    data[j] = temp

def conditionally_swap(data, i, j):
    if data[i] > data[j]:
        swap(data, i, j)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
